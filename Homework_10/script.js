'use strict';

const warningEmail = document.getElementById('warning-email');
const warningPassword = document.getElementById('warning-password');
const warningCheckbox = document.getElementById('warning-checkbox');
const inputEmail = document.getElementById('email');
const inputPassword = document.getElementById('password');
const inputCheckbox = document.getElementById('checkbox')
const checkboxMark = document.querySelector('.form__checkbox-mark');
const emailLabel = inputEmail.parentElement.querySelector('.form__input-label');
const passwordLabel = inputPassword.parentElement.querySelector('.form__input-label');
const button = document.querySelector('.form__button');
const formBlockEmail = document.querySelector('.form__block-email')
const formBlockPassword = document.querySelector('.form__block-password');
const formBlockCheckbox = document.querySelector('.form__checkbox-wrapper');
let isMailValid;
let isPasswordValid;
let isChecked;

function validation () {
    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    if (inputEmail.value === '') {
        warningEmail.innerHTML = 'Поле обязательно для заполнения';
        inputEmail.classList.add('error');
        emailLabel.classList.add('error-label');
        formBlockEmail.classList.add('error-label');
        isMailValid = false;
    } else if (!validateEmail(inputEmail.value)) {
        warningEmail.innerHTML = "Email невалидный";
        inputEmail.classList.add('error');
        emailLabel.classList.add('error-label');
        formBlockEmail.classList.add('error-label');
        isMailValid = false;
    } else {
        warningEmail.innerHTML = '';
        inputEmail.classList.remove('error');
        emailLabel.classList.remove('error-label');
        formBlockEmail.classList.remove('error-label');
        isMailValid = true;
    }
    if (inputPassword.value === '') {
        warningPassword.innerHTML = 'Поле обязательно для заполнения';
        inputPassword.classList.add('error');
        passwordLabel.classList.add('error-label');
        formBlockPassword.classList.add('error-label');
        isPasswordValid = false;
    } else if (inputPassword.value.length < 8) {
        warningPassword.innerHTML = 'Пароль должен содержать как минимум 8 символов';
        inputPassword.classList.add('error');
        passwordLabel.classList.add('error-label');
        formBlockPassword.classList.add('error-label');
        isPasswordValid = false;
    } else {
        warningPassword.innerHTML = '';
        inputPassword.classList.remove('error');
        passwordLabel.classList.remove('error-label');
        formBlockPassword.classList.remove('error-label');
        isPasswordValid = true;
    }
    if (!inputCheckbox.checked) {
        warningCheckbox.innerHTML = 'Поле обязательно для заполнения';
        checkboxMark.classList.add('error');
        formBlockCheckbox.classList.add('error-label');
        isChecked = false;
    } else {
        warningCheckbox.innerHTML = '';
        checkboxMark.classList.remove('error');
        formBlockCheckbox.classList.add('error-label');
        isChecked = true;
    }
}

button.addEventListener('click', (event) => {
    event.preventDefault();
    validation();
    if (isMailValid && isPasswordValid && isChecked) {
        console.log({
            email: inputEmail.value,
            password: inputPassword.value
        })
    }
})
