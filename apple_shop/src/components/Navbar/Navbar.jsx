import React from "react";
import { AppBar, Toolbar, IconButton, Badge, MenuItem, Menu, Typography} from "@material-ui/core";
import { ShoppingCart} from "@material-ui/icons";
import { Link, useLocation } from 'react-router-dom'

import logo from '../../img/apple.jpg'
import useStyles from './styles'

const Navbar = ({ totalItems}) => {
    const classes = useStyles();
    const location = useLocation();


    return (
        <>
          <AppBar position="fixed" className={classes.appBar} color="inherit">
              <Toolbar>
                  <Typography component={Link} to="/cart" variant="h6" className={classes.title} color="inherit">
                      <img src={logo} alt="Apple Shop" height="25px" className={classes.image} />
                      Apple Shop
                  </Typography>
                  <div className={classes.grow}/>
                  {location.pathname==='/' && (
                  <div className={classes.button}>
                      <Link to="/cart">Перейти в корзину</Link>
                      <IconButton component={Link} to="/cart" aria-label="Показать товары в корзине" color="inherit">
                          <Badge badgeContent={totalItems} color='secondary'>
                              <ShoppingCart />
                          </Badge>
                      </IconButton>
                  </div> ) }
              </Toolbar>
          </AppBar>
        </>
    )
}
export default Navbar